import { Component, EventEmitter } from '@angular/core';
import { UploadOutput, UploadInput, UploadFile, humanizeBytes, UploaderOptions, UploadStatus } from 'ngx-uploader';
import { environment } from '../environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  formData: FormData;
  files: UploadFile[];
  uploadInput: EventEmitter<UploadInput>;
  humanizeBytes: Function;
  dragOver: boolean;
  options: UploaderOptions;
  uploadFinished: boolean;
  finalLink: string;
  error: boolean;
  errorMessage: string;

  constructor() {
    this.options = { concurrency: 1 };
    this.files = [];
    this.uploadInput = new EventEmitter<UploadInput>();
    this.humanizeBytes = humanizeBytes;
  }

  onUploadOutput(output: UploadOutput): void {
    this.error = false;

    if (output.type === 'allAddedToQueue') {
      const event: UploadInput = {
        type: 'uploadAll',
        url: environment.api + '/upload',
        method: 'POST',
        headers: {
          'authentication': 'mA20$APIuplood'
        }
      };

      this.uploadInput.emit(event);
    } else if (output.type === 'addedToQueue'  && typeof output.file !== 'undefined') {
      this.files.push(output.file);
    } else if (output.type === 'uploading' && typeof output.file !== 'undefined') {
      const index = this.files.findIndex(file => typeof output.file !== 'undefined' && file.id === output.file.id);
      this.files[index] = output.file;
    } else if (output.type === 'removed') {
      this.files = this.files.filter((file: UploadFile) => file !== output.file);
    } else if (output.type === 'dragOver') {
      this.dragOver = true;
    } else if (output.type === 'dragOut') {
      this.dragOver = false;
    } else if (output.type === 'drop') {
      this.dragOver = false;
    } else if (output.type === 'done') {
      
      if (output.file.response && output.file.response.result && output.file.response.result == "succesful upload") {
        // console.debug('upload successful');
        this.uploadFinished = true;
        this.finalLink = output.file.response.link;
      } else if (output.file.response && output.file.response.error) {
        this.error = true;
        this.errorMessage = output.file.response.error;
      }

      if (output.file.responseStatus == 500) {
        this.error = true;
        this.errorMessage = "Verarbeitungsfehler - Falsche / Fehlerhafte Daten ?";
      }
      
    }

    this.files = this.files.filter(file => file.progress.status !== UploadStatus.Done);

    // console.debug('output.type',output);
  }

  startUpload(): void {
    const event: UploadInput = {
      type: 'uploadAll',
      url: environment.api + '/upload',
      method: 'POST',
      data: { foo: 'bar' }
    };

    this.uploadInput.emit(event);
  }

  cancelUpload(id: string): void {
    this.uploadInput.emit({ type: 'cancel', id: id });
  }

  removeFile(id: string): void {
    this.uploadInput.emit({ type: 'remove', id: id });
  }

  removeAllFiles(): void {
    this.uploadInput.emit({ type: 'removeAll' });
  }
}